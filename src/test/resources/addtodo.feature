Feature: Add Todo
  Scenario: Create todo by POST /todos
    Given ensure rest endpoint of "http://35.192.9.9:8080" is up
    When a POST request to /todos is made
    And the request body is
      """
{
  "targetDate": "2020-005-20",
  "description": "Test",
  "user": "Avinash",
  "done": false
}
      """
    Then a 201 response is returned within 2000ms